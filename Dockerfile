FROM alpine:latest
MAINTAINER endre.karlson@gmail.com


RUN apk update
RUN apk update && apk add ca-certificates && update-ca-certificates && apk add openssl wget
RUN sh -c 'wget "https://caddyserver.com/download/build?os=linux&arch=amd64&features=" -O /tmp/caddy.tar.gz'
RUN tar xvf /tmp/caddy.tar.gz -C /tmp
RUN mv /tmp/caddy /usr/bin/


WORKDIR /root
COPY Caddyfile /root
COPY dist /root/app

#ENTRYPOINT ["caddy"]
