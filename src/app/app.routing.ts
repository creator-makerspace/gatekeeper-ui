import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './auth/login.component';
import { LogoutComponent } from './auth/logout.component';
import { OpenIDComponent } from './auth/openid.component';

import { HomeComponent } from './home/home.component';
import { ErrorComponent } from './error.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { PointsComponent } from './point/point.component';
import { PointListComponent } from './point/point-list.component';



export const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'error',
    component: ErrorComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'logout',
    component: LogoutComponent
  },
  {
    path: 'openid',
    component: OpenIDComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },

  {
    path: 'points',
    component: PointsComponent,
    children: [
      {
        path: 'list',
        component: PointListComponent
      }
    ]
  },
];

export const routing = RouterModule.forRoot(routes);
