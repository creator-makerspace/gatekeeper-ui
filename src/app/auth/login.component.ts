import { Component, OnInit } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  isReady = false;

  constructor(private authSvc: OAuthService) { }

  ngOnInit() {
    const self = this;

    this.authSvc.loadDiscoveryDocument().then(() => {
      this.isReady = true
    })
  }

  gotoProvider() {
    this.authSvc.initImplicitFlow();
  }
}
