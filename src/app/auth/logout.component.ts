import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';

import {Observable} from 'rxjs';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
})
export class LogoutComponent implements OnInit {
  constructor(private authSvc: OAuthService, private router: Router) { }

  ngOnInit() {
    if (this.authSvc.logoutUrl !== '') {
      this.authSvc.logOut();

      Observable.timer(1000).subscribe(
        () => {
          this.router.navigate(['/']);
        }
      );
    }

  }
}
