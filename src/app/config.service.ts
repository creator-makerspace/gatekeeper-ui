import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';


@Injectable()
export class ConfigSvc {
  public cfg: any = null;
  
  constructor(private http: Http) {
  }

  has(key: string) {
    return this.cfg.hasOwnProperty(key);
  }

  get(key: string) {
    return this.cfg[key];
  }

  private fetch(): Observable<any> {
    let url = window.location.protocol + '//' + window.location.host;

    return this.http.get(url + '/assets/config.json')
      .map((res: Response) => res.json());
  }

  load(): Promise<any> {
    return this.fetch()
      .toPromise()
      .then((data: any) => {
        this.cfg = data
      })
      .catch((err: any) => Promise.resolve());
  }
}
