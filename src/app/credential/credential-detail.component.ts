import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {CredentialSvc} from '../credentials.service';

@Component({
  selector: 'app-credential-detail',
  templateUrl: './credential-detail.component.html',
})
export class CredentialDetailComponent {
  credential: any = {};

  constructor(creds: CredentialSvc, private route: ActivatedRoute) {
    this.route.params
      .mergeMap(
        i => creds.getCredential(i["id"])
      ).subscribe(
        data => this.credential = data
      );
  }
}
