import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {GrantsComponent} from './grants.component';
import {CredentialsComponent} from './credential.component';
import {CredentialCreateComponent} from './credential-create.component';
import {CredentialDetailComponent} from './credential-detail.component';
import {CredentialListComponent} from './credential-list.component';

import {SharedModule} from '../shared/shared.module';

import {routing} from './credential.routing';

@NgModule({
  declarations: [
    CredentialsComponent,
    CredentialCreateComponent,
    CredentialDetailComponent,
    CredentialListComponent,
    GrantsComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    routing,
  ]
})
export class CredentialModule {
}
