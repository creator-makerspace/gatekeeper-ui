import {RouterModule} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';

import { CredentialsComponent} from './credential.component';
import { CredentialCreateComponent} from './credential-create.component';
import { CredentialDetailComponent} from './credential-detail.component';
import { CredentialListComponent} from './credential-list.component';

export const routing: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'credentials',
    component: CredentialsComponent,
    children: [
      {
        path: 'create',
        component: CredentialCreateComponent
      },
      {
        path: 'list',
        component: CredentialListComponent
      },
      {
        path: ':id',
        component: CredentialDetailComponent
      }
    ]
  },
]);
