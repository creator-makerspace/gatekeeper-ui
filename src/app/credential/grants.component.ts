import {Component, Input, OnChanges} from '@angular/core';

import {Observable} from 'rxjs';

import {APIClient} from '../apiclient.service';
import {PointSvc} from '../points.service';

@Component({
  templateUrl: './grants.component.html',
  selector: 'app-credential-grants',
})
export class GrantsComponent implements OnChanges {
  @Input() credentialId: string;

  pointInfo = {};
  adding = false;

  grants = [];
  points = [];
  selectedPoint: any = null;
  selectedGrants: any = [];

  constructor(private api: APIClient, private pointSvc: PointSvc) {
  }

  refresh() {
    this.selectedGrants = [];

    this.api.get('/grants', { 'credential_id': this.credentialId })
      .map(res => res.grants)
      .do(grants => this.grants = grants)
      .mergeMap(grants => Observable.from(grants))
      .map(grant => grant['point_id'])
      .distinct()
      .mergeMap(
        id => this.pointSvc.getPoint(id)
      )
      .subscribe(grant => this.pointInfo[grant['id']] = grant);
  }

  add($event) {
    $event.preventDefault();
    this.adding = true;

    this.selectedPoint = null;

    this.pointSvc.getPoints().subscribe(
      points => this.points = points
    );
  }

  submit() {
    let grant = {
      credential_id: this.credentialId,
      point_id: this.selectedPoint
    };

    this.api.post('/grants', grant).subscribe(
      () => {
        this.refresh();
        this.adding = false;
      }
    );
  }

  revoke($event) {
    $event.preventDefault();

    Observable.from(this.selectedGrants)
      .flatMap(i => this.api.delete('/grants/' + i))
      .finally(() => this.refresh())
      .subscribe(i => i);
  }

  checkedChange($event) {
    if ($event) {
      this.selectedGrants = [];
    }
  }

  ngOnChanges() {
    if (this.credentialId !== undefined) {
      this.refresh();
    }
  }
}
