import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { APIClient } from './apiclient.service';

@Injectable()
export class CredentialSvc {
  constructor(private api: APIClient) { }

  getCredential(cid: string) {
    return this.api.get('/credentials/' + cid);
  }

  getCredentials() {
    return this.api.get('/credentials').map(resp => resp.credentials);
  }

  create(value: any) {
    return this.api.post('/credentials', value);
  }

  delete(cid: string) {
    return this.api.delete('/credentials/' + cid);
  }
}
