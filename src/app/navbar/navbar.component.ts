import {Component} from '@angular/core';
import {Http} from '@angular/http';

import {ConfigSvc} from '../config.service';
import { OAuthService } from 'angular-oauth2-oidc';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent {
  status: { isopen: boolean } = { isopen: false };
  authed = false;
  constructor(public configService: ConfigSvc, public authSvc: OAuthService) {
  }
}
