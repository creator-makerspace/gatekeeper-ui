import {RouterModule} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';

import { PointsComponent} from './point.component';
import { PointListComponent} from './point-list.component';

export const routing: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'points',
    component: PointsComponent,
    children: [
      {
        path: 'list',
        component: PointListComponent
      },
    ]
  },
]);
