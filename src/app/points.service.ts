import { Injectable } from '@angular/core';

import { APIClient } from './apiclient.service';

@Injectable()
export class PointSvc {
  constructor(private api: APIClient) { }

  getPoint(cid: string) {
    return this.api.get(`/points/${cid}`);
  }

  getPoints() {
    return this.api.get(`/points`).map(data => data.points);
  }
}
