import {Component, Input, OnChanges, OnInit} from '@angular/core';

@Component({
  selector: 'app-btn-expander',
  template: `<i class="fa fa-{{ class }}"></i>`
})
export class BtnExpanderComponent implements OnChanges, OnInit {
  @Input() activeClass: string = 'arrow-down';
  @Input() inactiveClass: string = 'arrow-left';
  @Input() state: boolean = false;

  private class: string;

  ngOnInit() {
    this.class = this.inactiveClass;
    console.log(this.class);
  }

  ngOnChanges() {
    if (this.state === undefined) {
      return;
    }

    this.class = this.state ? this.activeClass : this.inactiveClass;
  }
}
