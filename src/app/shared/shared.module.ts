import {NgModule} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import {StateIconComponent} from './state-icon/state-icon.component';
import {ActionCheckboxAllComponent, ActionCheckboxComponent} from './action-checkbox/action-checkbox.component';
import {BtnExpanderComponent} from './btn-expander/btn-expander.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    ActionCheckboxAllComponent,
    ActionCheckboxComponent,
    BtnExpanderComponent,
    StateIconComponent,
  ],
  exports: [
    ActionCheckboxAllComponent,
    ActionCheckboxComponent,
    BtnExpanderComponent,
    StateIconComponent,

    // Modules
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class SharedModule {
}
