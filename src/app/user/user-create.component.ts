import {Component} from '@angular/core';
import {Router} from '@angular/router';

import {
  FormBuilder,
  FormGroup,
  Validators,
  AbstractControl
} from '@angular/forms';

import {UserSvc} from '../users.service';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
})
export class UserCreateComponent {
  myForm: FormGroup;
  name: AbstractControl;
  enabled: AbstractControl;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private users: UserSvc
  ) {
    this.myForm = this.fb.group({
      name: ['', Validators.required],
      enabled: [true, Validators.required],
    });

    this.name = this.myForm.controls['name'];
    this.enabled = this.myForm.controls['enabled'];
  }

  onSubmit(value: any) {
    this.users.create(value).subscribe(
      () => {
        this.router.navigate(['/users/list']);
      }
    );
  }
}
