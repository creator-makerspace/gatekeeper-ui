import {Component} from '@angular/core';
import {Observable} from 'rxjs/Observable';

import {UserSvc} from '../users.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
})
export class UserListComponent {
  users: Array<any> = [];
  selected: Array<any> = [];

  constructor(private userSvc: UserSvc) {
    this.refresh();
  }

  refresh() {
    this.selected = [];

    this.userSvc.getUsers().subscribe(
      data => this.users = data
    );
  }

  checkedChange($event) {
    if ($event) {
      this.selected = [];
    }
  }

  delete() {
    Observable.from(this.selected)
      .flatMap(i => this.userSvc.delete(i))
      .finally(() => this.refresh())
      .subscribe();
  }
}
