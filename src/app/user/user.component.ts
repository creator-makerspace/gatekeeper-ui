import {Component} from '@angular/core';

import {APIClient} from '../apiclient.service';
import {UserSvc} from '../users.service';

@Component({
  selector: 'app-users',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  providers: [APIClient, UserSvc],
})
export class UsersComponent {
}
