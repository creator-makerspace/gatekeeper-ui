import {RouterModule} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';

import { UsersComponent} from './user.component';
import { UserCreateComponent} from './user-create.component';
import { UserDetailComponent} from './user-detail.component';
import { UserListComponent} from './user-list.component';

export const routing: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'users',
    component: UsersComponent,
    children: [
      {
        path: 'create',
        component: UserCreateComponent
      },
      {
        path: 'list',
        component: UserListComponent
      },
      {
        path: ':id',
        component: UserDetailComponent
      }
    ]
  }
]);
