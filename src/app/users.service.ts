import { Injectable } from '@angular/core';

import { APIClient } from './apiclient.service';

@Injectable()
export class UserSvc {
  constructor(private api: APIClient) { }

  getUser(uid: string) {
    return this.api.get(`/users/${uid}`);
  }

  getUsers() {
    return this.api.get(`/users`).map(data => data.users);
  }

  create(value: any) {
    return this.api.post('/users', value);
  }

  delete(uid: string) {
    return this.api.delete('/users/' + uid);
  }
}
